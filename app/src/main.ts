import { NestFactory } from '@nestjs/core';
import { ExpressAdapter, NestExpressApplication } from '@nestjs/platform-express';
import { Express } from 'express';
import * as express from 'express';

import { AppModule } from './app.module';
import { LoggerService } from './shared/services/logger.service';

async function bootstrap(expressInstance: Express) {
  const app = await NestFactory.create<NestExpressApplication>(AppModule, new ExpressAdapter(expressInstance), { cors: true });
  app.useLogger(app.get(LoggerService));
  await app.listen(3000);
}

export const server: Express = express();
export const main = bootstrap(server);
