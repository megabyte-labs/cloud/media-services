import * as AWS from 'aws-sdk';
import * as admin from 'firebase-admin';

export const AppConfig = {
    acoustIDKey: 'ryBDMLvCF1',
    digitalOcean: {
        spacesConfig: {
            accessKeyId: '6EXLLDTBKC7ZXPNTQKYM',
            endpoint: new AWS.Endpoint('nyc3.digitaloceanspaces.com'),
            secretAccessKey: 'DkCaXj769ClpsM94cY5XeSeatOh9oTahdZYiuVqwF0A'
        },
        streamBucket: 'videoblobs'
    },
    firebase: {
        'sites': ['musicblobs.com', 'videoblobs.com'],
        'musicblobs.com': {
            accountKey: admin.credential.cert(require('./assets/musicblobs.json')),
            app: {
                apiKey: 'AIzaSyCki70PKXOKif7Rj8yVeQamIFgWjd_-4t4',
                authDomain: 'music-blobs.firebaseapp.com',
                databaseURL: 'https://music-blobs.firebaseio.com',
                projectId: 'music-blobs',
                storageBucket: 'music-blobs.appspot.com',
                messagingSenderId: '103214312989',
            }
        },
        'videoblobs.com': {
            accountKey: admin.credential.cert(require('./assets/videoblobs.json')),
            app: {
                apiKey: 'AIzaSyAGUg968imiO5ve9z8DxbKfRmFt6cgb5CE',
                authDomain: 'video-blobs.firebaseapp.com',
                databaseURL: 'https://video-blobs.firebaseio.com',
                projectId: 'video-blobs',
                storageBucket: 'video-blobs.appspot.com',
                messagingSenderId: '486645527249',
            }
        }
    },
    key: 'V3PuQUlMVIWJGuCThN2qDVMFxJ1YnFP4LRAFEx4958uAgZDh4V'
};
