import { IsString, Matches } from 'class-validator';

export class YouTubeDashDTO {
    @IsString()
    @Matches(/[a-zA-Z0-9_-]{11}/)
    readonly id: string;
}