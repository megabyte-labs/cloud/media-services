import { Module } from '@nestjs/common';

import { SharedModule } from '../shared/shared.module';
import { YouTubeDashController } from './youtube-dash.controller';
import { YouTubeDashService } from './youtube-dash.service';

@Module({
  controllers: [
    YouTubeDashController
  ],
  imports: [
    SharedModule
  ],
  providers: [
    YouTubeDashService
  ]
})
export class YouTubeDashModule {}
