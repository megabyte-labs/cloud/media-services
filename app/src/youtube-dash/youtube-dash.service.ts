import { Injectable } from '@nestjs/common';
import * as YouTubeDownload from '@microlink/youtube-dl';
import * as child from 'child_process';
import * as fs from 'fs';
import * as rimraf from 'rimraf';
import * as path from 'path';

import { AppConfig } from '../app.config';
import { AppService } from '../app.service';
import { DashUpdates } from './interfaces/dash-updates.interface';
import { LoggerService } from '../shared/services/logger.service';
import { MediaData } from './interfaces/media-data.interface';
import { SubtitleData } from './interfaces/subtitle-data.interface';
import { S3Class } from '../s3/s3.class';

@Injectable()
export class YouTubeDashService {
    private dashDir;
    private sourceDir;
    private subDir;

    constructor(
        private appService: AppService,
        private log: LoggerService
    ) { }

    public async acquireDashFiles(id: string) {
        const updates: DashUpdates = {
            dash: false,
            dashProcessTime: new Date().getTime(),
            subtitles: []
        };
        try {
            this.setDirs(id);
            const files = await Promise.all([
                this.downloadAudio(id),
                this.downloadSubtitles(id),
                this.downloadVideo(id)
            ]);
            const audio = files[0];
            const subs = files[1];
            const video = files[2];
            await this.generateDashFiles(id, video, audio);
            fs.mkdirSync(path.join(this.dashDir, id, 'subs'));
            this.moveSubtitles(subs.files);
            rimraf.sync(this.subDir);
            rimraf.sync(this.sourceDir);
            const S3 = new S3Class(this.log, AppConfig.digitalOcean.spacesConfig);
            await S3.uploadFolder(this.dashDir, AppConfig.digitalOcean.streamBucket);
            updates.dash = true;
            updates.subtitles = subs.languages;
        } catch (e) {
            this.log.error('Failed to acquire DASH files for YouTube video with ID of ' + id, e);
            updates.error = e;
        }
        this.rmDirs();
        await this.appService['videoblobs.com'].db.collection('media').doc(id).set(updates, { merge: true });
    }

    private downloadAudio(id: string): Promise<MediaData[]> {
        return new Promise((resolve, reject) => {
            const audioFiles: MediaData[] = [];
            YouTubeDownload.exec(
                id,
                [
                    '-f',
                    'all[vcodec=none]',
                    '-o',
                    '%(format_id)s.%(format_note)s.%(ext)s'
                ],
                { cwd: this.sourceDir },
                async (error) => {
                    if (error) {
                        this.log.error('Error downloading videos from YouTube for video ID ' + id, error);
                        reject('audio-download');
                    } else {
                        this.log.debug('Audio finished downloading');
                        const audios = fs.readdirSync(this.sourceDir);
                        for (const audio of audios) {
                            const fileType = audio.substr(audio.length - 4);
                            if (fileType === '.m4a') {
                                audioFiles.push({
                                    order: audio.split('.')[0],
                                    name: audio
                                });
                            }
                        }
                        audioFiles.sort((a, b) => {
                            return parseInt(b.order, 10) - parseInt(a.order, 10);
                        });
                        if (!audioFiles.length) {
                            reject('audio-length');
                        }
                        resolve(audioFiles);
                    }
                }
            );
        });
    }

    private downloadSubtitles(id: string): Promise<{ files: SubtitleData[], languages: string[] }> {
        return new Promise((resolve, reject) => {
            const subFiles: SubtitleData[] = [];
            const subLangs: string[] = [];
            YouTubeDownload.exec(
                id,
                [
                    '--skip-download',
                    '--write-auto-sub',
                    '--all-subs',
                    '--sub-format',
                    'vtt',
                    '--write-sub',
                    '-o',
                    '%(id)s.%(format_note)s.%(ext)s',
                ],
                { cwd: this.subDir },
                (error, output) => {
                    if (error) {
                        this.log.error('Error downloading subtitles from YouTube for video ID ' + id, error);
                        reject('subtitle-download');
                    } else {
                        this.log.debug('Subtitles finished downloading');
                        const subs = fs.readdirSync(this.subDir);
                        for (const sub of subs) {
                            const isSub = sub.substr(sub.length - 4) === '.vtt';
                            if (isSub) {
                                const split = sub.split('.');
                                if (split[3] === 'vtt') {
                                    const fileName = split[2] + '.' + split[3];
                                    subFiles.push({
                                        destination: path.join(this.dashDir, id, 'subs', fileName),
                                        location: path.join(this.subDir, sub)
                                    });
                                    subLangs.push(split[2]);
                                } else {
                                    this.log.error('Unrecognized subtitle name format', null);
                                    reject('subtitle-name');
                                }
                            }
                        }
                        resolve({
                            files: subFiles,
                            languages: subLangs
                        });
                    }
                }
            );
        });
    }

    private downloadVideo(id: string): Promise<MediaData[]> {
        return new Promise((resolve, reject) => {
            const videoFiles: MediaData[] = [];
            YouTubeDownload.exec(
                id,
                [
                    '-f',
                    'all[acodec=none]',
                    '-o',
                    '%(format_id)s.%(format_note)s.%(ext)s'
                ],
                { cwd: this.sourceDir },
                async (error) => {
                    if (error) {
                        this.log.error('Error downloading videos from YouTube for video ID ' + id, error);
                        reject('video-download');
                    } else {
                        this.log.debug('Videos finished downloading');
                        const videos = fs.readdirSync(this.sourceDir);
                        for (const video of videos) {
                            const fileType = video.substr(video.length - 4);
                            if (fileType === '.mp4') {
                                videoFiles.push({
                                    order: video.split('.')[0],
                                    name: video
                                });
                            }
                        }
                        videoFiles.sort((a, b) => {
                            return parseInt(a.order, 10) - parseInt(b.order, 10);
                        });
                        if (!videoFiles.length) {
                            reject('video-length');
                        }
                        resolve(videoFiles);
                    }
                }
            );
        });
    }

    private generateDashFiles(id: string, dashFiles: MediaData[], audioFiles: MediaData[]) {
        return new Promise((resolve, reject) => {
            const dashParams = [];
            for (const dashFile of dashFiles) {
                dashParams.push(path.join(this.sourceDir, dashFile.name));
            }
            for (const audioFile of audioFiles) {
                const split = audioFile.name.split('.');
                const joined = split[0] + '.m4a';
                fs.renameSync(path.join(this.sourceDir, audioFile.name), path.join(this.sourceDir, joined));
                dashParams.push(path.join(this.sourceDir, joined));
            }
            const command = path.join(
                __dirname,
                '..',
                'assets',
                'binary',
                'bento4',
                AppService.platform,
                'bin',
                'mp4dash'
            ) + ' -v -o ' + path.join(this.dashDir, id) + ' ' + dashParams.join(' ');
            child.exec(command, (error, stdout, stderr) => {
                if (error) {
                    this.log.error('Error executing the command "' + command + '"', error);
                    reject('dash-command');
                } else {
                    resolve();
                }
            });
        });
    }

    private moveSubtitles(subs: SubtitleData[]) {
        for (const sub of subs) {
            fs.renameSync(sub.location, sub.destination);
        }
    }

    private rmDirs() {
        if (fs.existsSync(this.dashDir)) {
            rimraf.sync(this.dashDir);
        }
        if (fs.existsSync(this.sourceDir)) {
            rimraf.sync(this.sourceDir);
        }
        if (fs.existsSync(this.subDir)) {
            rimraf.sync(this.subDir);
        }
    }

    private setDirs(id: string) {
        this.dashDir = path.join(AppService.tmp, id + '_' + 'dash');
        this.sourceDir = path.join(AppService.tmp, id + '_' + 'source');
        this.subDir = path.join(AppService.tmp, id + '_' + 'subs');
        this.rmDirs();
        fs.mkdirSync(this.dashDir);
        fs.mkdirSync(this.sourceDir);
        fs.mkdirSync(this.subDir);
    }
}
