export interface DashUpdates {
    dash: boolean;
    dashProcessTime: number;
    error?: string;
    subtitles: string[];
}
