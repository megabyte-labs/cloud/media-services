export interface SubtitleData {
    destination: string;
    location: string;
}
