import { Body, Controller, Post, UseGuards, UsePipes, ValidationPipe } from '@nestjs/common';

import { APIGuard } from '../shared/guards/api.guard';
import { YouTubeDashDTO } from './dto/youtube-dash.dto';
import { YouTubeDashService } from './youtube-dash.service';

@Controller('api/dash')
export class YouTubeDashController {

    constructor(
        private youtubeDash: YouTubeDashService
    ) { }

    @Post('youtube')
    @UseGuards(APIGuard)
    @UsePipes(ValidationPipe)
    public async acquireDashFiles(@Body() body: YouTubeDashDTO) {
        await this.youtubeDash.acquireDashFiles(body.id);
    }
}
