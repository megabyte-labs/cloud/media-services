import { IsString } from 'class-validator';

export class AcoustIDDTO {
    @IsString()
    readonly fileid: string;
}