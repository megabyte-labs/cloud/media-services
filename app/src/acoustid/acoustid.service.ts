import { Injectable } from '@nestjs/common';
import * as acoustid from 'acoustid';
import * as fs from 'fs';
import * as path from 'path';
import * as rimraf from 'rimraf';

import { AppService } from '../app.service';
import { AppConfig } from '../app.config';

@Injectable()
export class AcoustIDService {
    private audioDir = path.join('/tmp/', 'audio');

    constructor(
        private appService: AppService
    ) { }

    public async getID(id: string) {
        try {
            const options = {
                destination: path.join(this.audioDir, id + '.mp3'),
                validation: false
            };
            this.resetDownloadDir();
            await this.appService.storage['musicblobs.com']
                .bucket('audio.musicblobs.com')
                .file(decodeURIComponent(id) + '.mp3')
                .download(options);
            const results = await this.acquireID(id);
            this.resetDownloadDir();
            return {
                results
            };
        } catch (e) {
            this.appService.badRequest('AcoustID failed for file ID "' + id + '"', typeof e === 'string' ? e : null, e);
        }
    }

    private acquireID(id: string) {
        return new Promise((resolve, reject) => {
            acoustid(path.join(this.audioDir, id + '.mp3'), { key: AppConfig.acoustIDKey }, (error, results) => {
                if (error) {
                    reject('acoustid-failed');
                } else {
                    resolve(results);
                }
            });
        });
    }

    private resetDownloadDir() {
        if (fs.existsSync(this.audioDir)) {
            rimraf.sync(this.audioDir);
        }
        fs.mkdirSync(this.audioDir);
    }
}
