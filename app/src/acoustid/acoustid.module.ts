import { Module } from '@nestjs/common';

import { AcoustIDController } from './acoustid.controller';
import { AcoustIDService } from './acoustid.service';
import { SharedModule } from '../shared/shared.module';

@Module({
    controllers: [
      AcoustIDController
    ],
    imports: [
      SharedModule
    ],
    providers: [
      AcoustIDService
    ]
  })
export class AcoustIDModule {}
