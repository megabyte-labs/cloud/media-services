import { Body, Controller, Post, UseGuards } from '@nestjs/common';

import { AcoustIDService } from './acoustid.service';
import { APIGuard } from '../shared/guards/api.guard';

@Controller('api/acoustid')
export class AcoustIDController {

    constructor(
        private acoustID: AcoustIDService
    ) { }

    @Post()
    @UseGuards(APIGuard)
    public async getID(@Body() body: AcoustIDDTO) {
        await this.acoustID.getID(body.fileid);
    }
}
