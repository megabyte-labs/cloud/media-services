import { Injectable, Logger } from '@nestjs/common';

@Injectable()
export class LoggerService extends Logger {

    constructor() {
        super();
    }

    public debug(message: any, ...args: any) {
        if (args) {
            console.error(message, ...args);
        } else {
            console.error(message);
        }
    }

    public error(message: any, ...error: any) {
        if (error) {
            console.error(message, ...error);
        } else {
            console.error(message);
        }
    }

    public log(message: any, ...args: any) {
        if (args) {
            console.log(message, ...args);
        } else {
            console.log(message);
        }
    }

}
