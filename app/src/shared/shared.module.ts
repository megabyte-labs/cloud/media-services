import { Module } from '@nestjs/common';

import { AppService } from '../app.service';
import { LoggerService } from './services/logger.service';

@Module({
    exports: [
        AppService,
        LoggerService
    ],
    providers: [
        AppService,
        LoggerService
    ],
})
export class SharedModule {}
