import * as AWS from 'aws-sdk';
import * as fs from 'fs';
import * as path from 'path';

import { LoggerService } from '../shared/services/logger.service';

export class S3Class {
    private log: LoggerService;
    private s3: AWS.S3;
    private walkSyncPromises = [];

    constructor(log: LoggerService, s3Options: any) {
        this.log = log;
        this.s3 = new AWS.S3(s3Options);
    }

    public async uploadFolder(s3Path: string, bucketName: string) {
        this.walkSync(s3Path, (filePath, stat) => {
            return new Promise((resolve, reject) => {
                const bucketPath = filePath.substring(s3Path.length + 1);
                const params = {
                    Bucket: bucketName,
                    Key: bucketPath.replace(/\\/g, '/'), // Replace for Windows slash schema
                    Body: fs.readFileSync(filePath),
                    ACL: 'public-read'
                };
                this.s3.putObject(params, (err, data) => {
                    if (err) {
                        this.log.error('ERROR uploading to ' + bucketPath, err);
                        reject();
                    } else {
                        this.log.debug(data);
                        this.log.log('Successfully uploaded ' + bucketPath + ' to ' + bucketName);
                        resolve();
                    }
                });
            });
        });
        for (const promise of this.walkSyncPromises) {
            await promise.callback(promise.filePath, promise.stat);
        }
    }

    private walkSync(currentDirPath: string, callback: any) {
        fs.readdirSync(currentDirPath).forEach(name => {
            const filePath = path.join(currentDirPath, name);
            const stat = fs.statSync(filePath);
            if (stat.isFile()) {
                this.walkSyncPromises.push({
                    callback,
                    filePath,
                    stat
                });
            } else if (stat.isDirectory()) {
                this.walkSync(filePath, callback);
            }
        });
    }
}
