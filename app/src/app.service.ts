import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import * as firebase from 'firebase-admin';

import { AppConfig } from './app.config';
import { LoggerService } from './shared/services/logger.service';

@Injectable()
export class AppService {
  static platform: 'linux' | 'mac' | 'windows' | 'other';
  static tmp = '/tmp/';

  public db = {};
  public fcm = {};
  public firebase = {};
  public storage = {};

  constructor(
    private log: LoggerService
  ) {
    this.setPlatform();
    this.log.log('Firebase apps initializing');
    for (const site of AppConfig.firebase.sites) {
      this.firebase[site] = {};
      const app = firebase.initializeApp({
        credential: AppConfig.firebase[site].accountKey,
        databaseURL: AppConfig.firebase[site].app.databaseURL
      });
      this.firebase[site].app = app;
      this.db[site] = app.firestore();
      this.fcm[site] = app.messaging();
      this.storage[site] = app.storage();
    }
  }

  public badRequest(message: string, code: string, error: any) {
    this.log.error(message, error);
    throw new HttpException({
      code,
      status: HttpStatus.BAD_REQUEST,
      error: message,
    }, 400);
  }

  private setPlatform() {
    const platform = process.platform;
    if (platform === 'win32') {
      AppService.platform = 'windows';
    } else if (platform === 'linux') {
      AppService.platform = 'linux';
    } else if (platform === 'darwin') {
      AppService.platform = 'mac';
    } else {
      AppService.platform = 'other';
    }
  }
}
