import { Module } from '@nestjs/common';

import { AcoustIDModule } from './acoustid/acoustid.module';
import { AppController } from './app.controller';
import { SharedModule } from './shared/shared.module';
import { YouTubeDashModule } from './youtube-dash/youtube-dash.module';


@Module({
  imports: [
    AcoustIDModule,
    SharedModule,
    YouTubeDashModule
  ],
  controllers: [
    AppController
  ]
})
export class AppModule {}
