FROM ubuntu:latest as builder
COPY app /root/app/
RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get install -y --no-install-recommends \
        build-essential \
        ca-certificates \
        curl \
        git \
        gnupg \
        make \
        software-properties-common \
        libchromaprint-tools \
    && curl -sL https://deb.nodesource.com/setup_9.x | bash \
    && apt-get -qq install -y nodejs \
    && npm install -g n \
    && n latest \
    && cd /root/app \
    && npm install

FROM ubuntu:latest

WORKDIR /opt/auxilary

COPY --from=builder /root/app/ /opt/auxilary/
COPY process.json /opt/auxilary/process.json

RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get install -y --no-install-recommends \
        curl \
        gnupg \
        software-properties-common \
        libchromaprint-tools \
    && curl -sL https://deb.nodesource.com/setup_9.x | bash \
    && apt-get -qq install -y nodejs \
    && npm install -g n pm2 \
    && n latest \
    && pm2 install pm2-logrotate

EXPOSE 3000

CMD ["pm2-docker", "process.json"]